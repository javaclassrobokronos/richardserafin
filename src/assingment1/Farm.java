package assingment1;

public class Farm {
	
	private example1.BarnYard _by;
		
	public Farm(){
	  _by=new example1.BarnYard();	
	  Pigs();
	  Pigs();
	  Pigs();
	}
	
	public void Butterflies(){
		example1.Butterfly bu;
		bu=new example1.Butterfly();
		_by.addButterfly(bu);
	}
	
	public void Pigs(){
		example1.Pig pg1;
		pg1=new example1.Pig();
		example1.Pig pg2;
		pg2=new example1.Pig();
		_by.addPig(pg1);
		_by.addPig(pg2);
		pg1.start();
		pg2.start();
	}	
	
	public void Chickens(){
		example1.Chicken cn1;
		cn1= new example1.Chicken();
		example1.Chicken cn2;
		cn2= new example1.Chicken();
		example1.Chicken cn3;
		cn3= new example1.Chicken();
		_by.addChicken(cn1);
		_by.addChicken(cn2);
		_by.addChicken(cn3);
		cn1.start();
		cn2.start();
	}	
	
}
