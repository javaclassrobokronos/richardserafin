package assingment2;

public class Zoo {
	private example1.BarnYard _by;
	private example1.BarnYard _by1;
	private example1.BarnYard _by2;
	
	public Zoo(){
		_by=new example1.BarnYard();
		_by1=new example1.BarnYard();
		_by2=new example1.BarnYard();
		chicken();
		chicken();
		pig();
		butterfly();
	}
	public void chicken(){
		example1.Chicken ch;
		ch=new example1.Chicken();
		_by.addChicken(ch);
		example1.Pig pi;
		pi=new example1.Pig();
		_by.addPig(pi);
		pig();
	}
	public void pig(){
		example1.Pig pi;
		pi=new example1.Pig();
		_by1.addPig(pi);
		example1.Chicken ch;
		ch=new example1.Chicken();
		_by1.addChicken(ch);
	}
	public void butterfly(){
		example1.Butterfly bu;
		bu=new example1.Butterfly();
		_by2.addButterfly(bu);
		example1.Butterfly bi;
		bi=new example1.Butterfly();
		_by2.addButterfly(bi);	
	}
	
		

}
