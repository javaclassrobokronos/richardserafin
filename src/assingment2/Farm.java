package assingment2;

public class Farm {

	private example1.BarnYard _by;
	private example1.Chicken _ch;
	private example1.Butterfly _bu;
	private example1.Pig _pi;
	
	public Farm(){
		_by=new example1.BarnYard();
		go();
		add();
		start();
	}
	public void go(){
		_ch=new example1.Chicken();
		_bu=new example1.Butterfly();
		_pi=new example1.Pig();
	}
	public void add(){
		_by.addPig(_pi);
		_by.addChicken(_ch);
		_by.addButterfly(_bu);
	}
	public void start(){
		_pi.start();
		_ch.start();
		_bu.start();
	}
	
}
